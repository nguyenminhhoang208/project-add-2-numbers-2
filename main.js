class MyBigNumber {
	sum(stn1, stn2) {
		console.log(
			'thực hiện phép toán',
			stn1,
			' + ',
			stn2,
			' theo thứ tự từ phải qua trái như sau: '
		);

		let biennho = 0;
		let resultArr = []; // dùng để chứa kết quả mỗi bước tính
		let maxLengthOfStns = stn1.length > stn2.length ? stn1.length : stn2.length;

		// thêm phần từ 0 vào đầu chuỗi
		maxLengthOfStns += 1;

		while (stn1.length < maxLengthOfStns) {
			stn1 = '0' + stn1;
		}

		while (stn2.length < maxLengthOfStns) {
			stn2 = '0' + stn2;
		}

		console.log(
			'trước tiên ta thêm số 0 vào trước các số như sau: ',
			stn1,
			' + ',
			stn2,
			' sau đó: '
		);

		let result;
		do {
			// điều kiện dừng vòng lặp
			maxLengthOfStns--;

			// tính kết quả của phép cộng 2 số hạng và số nhớ
			// stn1[maxLengthOfStns] - -stn2[maxLengthOfStns] tự động convert sang kiểu number để tính toán
			result = stn1[maxLengthOfStns] - -stn2[maxLengthOfStns] + biennho;

			if (result === 0 && maxLengthOfStns === maxLengthOfStns) break;
			// log ra màn hình bước tính toán
			let mess =
				biennho !== 0
					? `lấy ${stn1[maxLengthOfStns]} + ${stn2[maxLengthOfStns]} + ${biennho} (số nhớ) được ${result}`
					: `lấy ${stn1[maxLengthOfStns]} + ${stn2[maxLengthOfStns]} được ${result}`;

			console.log(mess);

			// tính phần ghi kết quả và cập nhật biến nhớ

			resultArr.unshift(result % 10);
			biennho = Math.floor(result / 10);

			// log ra màn hình bước tính toán
			mess =
				biennho !== 0
					? `viết ${resultArr[0]} nhớ ${biennho}`
					: `viết ${resultArr[0]}`;
			console.log(mess);
			console.log('được kết quả là: ', resultArr.join(''));


		} while (maxLengthOfStns >= 0);
		console.log(
			'Vậy kết quả kết quả của phép cộng trên là: ',
			resultArr.join('')
		);
	}
}

const a = new MyBigNumber();
a.sum('1234', '897');

// thực hiện phép toán 1234  +  897  theo thứ tự từ phải qua trái như sau: 
// trước tiên ta thêm số 0 vào trước các số như sau:  01234  +  00897  sau đó: 
// lấy 4 + 7 được 11
// viết 1 nhớ 1
// được kết quả là:  1
// lấy 3 + 9 + 1 (số nhớ) được 13
// viết 3 nhớ 1
// được kết quả là:  31
// lấy 2 + 8 + 1 (số nhớ) được 11
// viết 1 nhớ 1
// được kết quả là:  131
// lấy 1 + 0 + 1 (số nhớ) được 2
// viết 2
// được kết quả là:  2131
// Vậy kết quả kết quả của phép cộng trên là:  2131
